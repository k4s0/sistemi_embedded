#include "Potentiometer.h"
#include "Arduino.h"

Potentiometer :: Potentiometer(int bot, int top) {
  this->pin = A0;
  this->bot = bot;
  this->top = top;
  pinMode(pin, INPUT);
}

int Potentiometer :: getValue() {
  return map(analogRead(pin), 0, 1023, bot, top);
}
