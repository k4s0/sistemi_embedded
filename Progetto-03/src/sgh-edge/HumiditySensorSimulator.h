#ifndef __HUMIDITYSENSORSIMULATOR__
#define __HUMIDITYSENSORSIMULATOR__
#include "Humidity.h"
#include "Potentiometer.h"
class HumiditySensorSimulator : public Humidity{
  public:
    HumiditySensorSimulator(int bot, int top);
    int getHumidity();

  private:
    Potentiometer *p;
};
#endif
