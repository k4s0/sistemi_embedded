#ifndef __HUMIDITY__
#define __HUMIDITY__
class Humidity {
  public:
    virtual int getHumidity() = 0;
};
#endif
