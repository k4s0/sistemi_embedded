#include "HumiditySensorSimulator.h"

HumiditySensorSimulator :: HumiditySensorSimulator(int bot, int top){
  this->p = new Potentiometer(bot, top);
}

int HumiditySensorSimulator :: getHumidity(){
  return this->p->getValue();
};
