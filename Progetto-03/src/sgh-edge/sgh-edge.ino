#include <ESP8266HTTPClient.h>
#include <ESP8266WiFi.h>
#include "HumiditySensorSimulator.h"

char* password = "SOPHI123456789";
char* ssid = "SOPHI";
Humidity* h = new HumiditySensorSimulator(0, 100);;
HTTPClient http;

void setup() {
  Serial.begin(115200);                 
  WiFi.begin(ssid, password);   
  Serial.println(WL_CONNECTED);
  while (WiFi.status() != WL_CONNECTED) { 
    delay(500);
    Serial.print(String(WiFi.status()));
  }

  Serial.println("Connected");
  WiFi.mode(WIFI_STA);
  Serial.print(WiFi.localIP());

}

void loop() {
  
  if(WiFi.status()== WL_CONNECTED){   //Check WiFi connection status
 
   HTTPClient http;    //Declare object of class HTTPClient
 
   http.begin("http://7489c53e.ngrok.io/server");      //Specify request destination
   http.addHeader("Content-Type", "application/x-www-form-urlencoded", false, true);  //Specify content-type header
   int httpCode = http.POST(String(h->getHumidity()));   //Send the request
   String payload = http.getString();                  //Get the response payload
 
   Serial.println(httpCode);   //Print HTTP return code
   Serial.println("http payload: "+payload);    //Print request response payload
 
   http.end();  //Close connection
   if(httpCode <= 0) http.errorToString(httpCode).c_str();
 
 }else{
 
    Serial.println("Error in WiFi connection");   
 
 }
 
  delay(500);  //Send a request every 30 seconds
}
