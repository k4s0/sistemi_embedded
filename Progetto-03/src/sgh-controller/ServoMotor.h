#ifndef __SERVOMOTOR__
#define __SERVOMOTOR__
#include "Arduino.h"
#include <ServoTimer2.h>

class ServoMotor {
  public:
    ServoMotor(int pwmPin);
    void setPosition(int angle);

  private:
    ServoTimer2 servo;
};

#endif
