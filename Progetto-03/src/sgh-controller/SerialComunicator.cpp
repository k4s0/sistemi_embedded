#include "SerialComunicator.h"

SerialComunicator::SerialComunicator(){}
MsgServiceBT msgServiceBT(3, 2);

void SerialComunicator :: init(int period){
  Task::init(period);
  lastSetting = setting;
  Serial.println("[SerialCounicator] Ready");
   
   msgServiceBT.init();  
   Serial.begin(9600);
   while (!Serial){}
   Serial.println("[Serial Bluetooth] ready to go."); 
}

void SerialComunicator :: tick(){
  messageFromBT();
  messageFromSGH_LA();
  messageToSend();
}

void SerialComunicator ::  messageFromBT(){ 
  if (msgServiceBT.isMsgAvailable() && connection == ConnectionState::CONNECTED) {
    Msg* msg = msgServiceBT.receiveMsg();
    String m = msg->getContent();
    if (m == "setManual"){
       setting = Setting::MANUAL;
    } else if(m == "setAuto"){
      setting = Setting::AUTO;
    } else if(m == "imc"){
      ack = true;
    }else if(setting == Setting::MANUAL) {
        if(m == "close"){
          //mando il messaggio per chiudere l'erogazione
          openPump = false;
        }else if(m == "open"){
          openPump = true;
          currFlowM = DEFAULT_FLOW_M;
        }else{
          int i = atoi( m.c_str() );
          i = i < 1 ? 1 : i;
          i = i > 5 ?  5 : i;
          currFlowM = i;
        }
    }
    delete msg;
  }
}
void SerialComunicator :: messageFromSGH_LA(){
    if(MsgService.isMsgAvailable()){
    Msg* msg = MsgService.receiveMsg();
    Serial.println("[ArduinoSerialComunicator] "+msg->getContent());
    String content = msg->getContent();
    if(setting == Setting::AUTO){
     if(content == "1" || content == "2" || content == "3"){
        openPump = true;
        currFlow = Flow::Pmin;
        currFlow = content == "2" ? Flow::Pmed : currFlow;
        currFlow = content == "3" ? Flow::Pmax : currFlow;
      } else if(content == "c"){
        openPump = false;
      }
    }
    if(content.indexOf('u')==0){
      msgServiceBT.sendMsg(Msg(content));
    }
  delete msg;
  }
}

void SerialComunicator :: messageToSend(){
  /*switching between defferent setting*/
  if(lastSetting == Setting::AUTO && setting == Setting::MANUAL){
    MsgService.sendMsg("m");
    openPump = false;
    timeout = false;
    msgServiceBT.sendMsg(Msg("Setting: MANUAL"));
  } else if(lastSetting == Setting::MANUAL && setting == Setting::AUTO){
    MsgService.sendMsg("a");
    msgServiceBT.sendMsg(Msg("Setting: AUTO"));
  }

  /*notify timeout*/
  if(timeout){
    timeout = false;
    MsgService.sendMsg("t");
  }

  //auc
  if(auc){
     msgServiceBT.sendMsg(Msg("auc"));
     auc=false;
  }
  lastSetting = setting;

}
