#ifndef __AUTO__
#define __AUTO__

#include "Task.h"
#include "config.h"
#include "Arduino.h"
#include "Enumeration.h"
#include "Led.h"
#include "Pump.h"

extern Setting setting;
extern User user;
extern Flow currFlow;
extern bool openPump;
extern bool timeout;
extern float currUmidity;

/**
 * Task for automatic sgh-controller behavior.
 */
class Auto : public Task{
  private:
    Led *L1;
    Pump *pump;
    bool toReset;
    int cTimeout;
    State state;
    void reset();
    void switchToManual();
  public:
  Auto(int pinL1, Pump *pump);
  void init(int period);
  void tick();

  
};

#endif
