#ifndef __CONNECTION__
#define __CONNECTION__

#include "config.h"
#include "Task.h"
#include "Enumeration.h"

extern Setting setting;
extern User user;
extern bool openPump;
extern bool auc;
extern bool ack;
extern ConnectionState connection;



class Connection : public Task{
  private: 
    int timeout;
    void switchNotConnected();
  public:
    Connection();
    void init(int period);
    void tick();
};

#endif
