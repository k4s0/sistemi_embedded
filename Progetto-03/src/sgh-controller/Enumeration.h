#ifndef __ENUMERATION__
#define __ENUMERATION__
enum class Setting {AUTO, MANUAL};
enum class User{DETECTED, NOT_FOUND};
enum class Flow {Pclose, Pmin, Pmed, Pmax};
enum class State{OPEN, CLOSE};
enum class ConnectionState{CONNECTED, NOT_CONNECTED};
#endif
