#ifndef __DETECTMOTION__
#define __DETECTMOTION__

#include "config.h"
#include "Task.h"
#include "Sonar.h"
#include "Enumeration.h"

extern Setting setting;
extern User user;

class DetectMotion : public Task{
  private:
    int pinTrig;
    int pinEcho;
    int count;
    Sonar *sonar;
  public:
    DetectMotion(int pinTrig, int pinEcho);
    void init(int period);
    void tick();
};

#endif
