#include "Arduino.h"
#include "Scheduler.h"
#include "DetectMotion.h"
#include "serialComunicator.h"
#include "Auto.h";
#include "Manual.h"
#include "Enumeration.h"
#include "config.h"
#include "Pump.h"
#include "Connection.h"

/**
 * Global variable for Task communication
 */
 
/*the setting system manual or auto*/
Setting setting;
/*if user is detected or no*/
User user;
/*current pump flow auto*/
Flow currFlow;
/*current pump flow manual*/
int currFlowM;
/*if is true the pump must be open*/
bool openPump;
/*true if timeout is turn on*/
bool timeout;
/* current umidity*/
float currUmidity;
/*for understand if mobile is connected by bluetooth or not*/
bool auc;
/*true if mobile is connected*/
bool ack;
/*connection state*/
ConnectionState connection;

Scheduler sched;

void setup() {
  Serial.begin(9600);
  setting = Setting::AUTO;
  user = User::NOT_FOUND;
  timeout = false;
  currFlow = Flow::Pmin;
  openPump = false;
  currUmidity = 1.0;
  connection = ConnectionState:: NOT_CONNECTED;

  sched.init(100);
  
  Pump *pump = new Pump(L2pin, SERVOPIN, MAX_FLOW_LM);
  ServoMotor sm = new ServoMotor(SERVOPIN);
  
  Task *dm = new DetectMotion(TrigPIN, EchoPIN);
  dm->init(TICK_DM);  

  Task *sc = new SerialComunicator();
  sc->init(TICK_SC);

  Task *a = new Auto(L1pin, pump);
  a->init(TICK_AUTO);

  Task *m = new Manual(Lmpin, pump);
  m->init(TICK_AUTO); 

  Task *c = new Connection();
  c->init(TICK_CONN); 
   
  sched.addTask(dm);
  sched.addTask(sc);
  sched.addTask(c);
  sched.addTask(m);
  sched.addTask(a);
}

void loop() {
  sched.schedule();
}
