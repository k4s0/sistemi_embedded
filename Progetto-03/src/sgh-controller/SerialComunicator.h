#ifndef __SERIALCOMUNICATOR__
#define __SERIALCOMUNICATOR__

#include "Task.h"
#include "MsgService.h"
#include "config.h"
#include "Arduino.h"
#include "Enumeration.h"
#include "SoftwareSerial.h"

extern Setting setting;
extern User user;
extern Flow currFlow;
extern int currFlowM;
extern bool openPump;
extern bool timeout;
extern float currUmidity;
extern ConnectionState connection;
extern bool auc;
extern bool ack;

class SerialComunicator : public Task{
  private:
    Setting lastSetting;
    void messageFromBT();
    void messageFromSGH_LA();
    void messageToSend();
    
  public:
    SerialComunicator();
    void init(int period);
    void tick();
};

#endif
