#ifndef __CONFIG__
#define __CONFIG__

/*configuration file*/

#define MAX_TASKS 10

#define __DEBUG_BUTTON__
//#define __DEBUG__
#define DIST 0.30
#define Tmax 5000 //5s

#define L1pin 13
#define L2pin 5
#define Lmpin 12
#define SERVOPIN 6
#define TrigPIN 7
#define EchoPIN 8

#define TICK_AUTO 100
#define TICK_DM 500
#define TICK_SC 100
#define TICK_CONN 500

#define MAX_FLOW_LM 5
#define DEFAULT_FLOW_M 2
#define TIMEOUT 3 

#endif
