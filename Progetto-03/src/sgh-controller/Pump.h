#ifndef __PUMP__
#define __PUMP__

#include "Enumeration.h"
#include "Led.h"
#include "ServoMotor.h"

class Pump {
  public:
    Pump(int pin, int pinPwm, int maxFlowLM);
    void openPump(int flow);
    void openPump(Flow flow);
    void closePump();
    
  private:
    Led *L;
    ServoMotor *sm;
    int maxFlowLM;
};
#endif
