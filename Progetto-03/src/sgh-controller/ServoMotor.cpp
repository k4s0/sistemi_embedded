#include "ServoMotor.h"
#include "Arduino.h"

ServoMotor :: ServoMotor(int pwmPin) {
  servo.attach(pwmPin);   
}

void ServoMotor :: setPosition(int angle) {
  float f = 180/2250 * angle;
  servo.write((int) f);
 // Serial.println("[ServoMotor]" + String(f));
}
