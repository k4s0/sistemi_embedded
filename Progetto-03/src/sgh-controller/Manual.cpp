#include "Task.h"
#include "config.h"
#include "Manual.h"

Manual :: Manual(int pinLm, Pump *pump){
  Lm = new Led(pinLm);
  this->pump = pump;
}

void Manual :: init(int period){
  Serial.println("[Manual] Ready");
  Task::init(period);
  reset();
}

void Manual :: tick(){
  if(setting == Setting::MANUAL){
    if(toReset) reset();
    switch(state){
      case State::CLOSE :
        if(openPump == true){
          pump->openPump(currFlowM);
          state = State :: OPEN;
        }
        break;
      case State::OPEN :
        if(openPump == true){
          pump->openPump(currFlowM);
        } else {
          pump->closePump();
          state = State :: CLOSE;
        }
    }
  }else if (setting == Setting::AUTO && !toReset){
    switchToAuto();
  }
}

void Manual ::reset(){
  state = State :: CLOSE;
  toReset = false;
  Lm->switchOn();
}

void Manual::switchToAuto(){
  toReset = true;
  Lm->switchOff();
  pump->closePump();
}
