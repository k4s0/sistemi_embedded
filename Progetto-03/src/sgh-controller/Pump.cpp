#include "Pump.h"
#include "Arduino.h"

Pump::Pump(int pin, int pinPwm, int maxFlowLM) {
  this->L = new Led(pin);
  this->sm = new ServoMotor(pinPwm);
  this->maxFlowLM = maxFlowLM;
}

void Pump :: openPump(int flow){
  float f = flow / maxFlowLM;
  int t = 64;
  t = flow == 2 ? 102 : t;
  t = flow == 3 ? 153 : t;
    t = flow == 4 ? 204 : t;
      t = flow == 5 ? 255 : t; 
  Serial.println("[Pump.cpp] fade " + String (t));
  L->fade(t);
  sm->setPosition((int)(f*180));
}

void Pump :: closePump(){
  L->switchOff();
  sm->setPosition(0);
}
void Pump :: openPump(Flow flow){
  float f = 0.10;
  f = flow == Flow::Pmed ? 0.66 : f;
  f = flow == Flow::Pmax ? 1 : f;
  f = flow == Flow::Pclose ? 0 : f;
  int t = (int)(f*255);
  int t1 = (int)(f*180);
  L->fade(t);
  sm->setPosition(t1);
}
