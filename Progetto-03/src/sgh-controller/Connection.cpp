#include "Connection.h"
#include "Arduino.h"


Connection :: Connection() {
  timeout = 0;
}

void Connection :: init(int period) {
  Task::init(period);
  Serial.println("[Connection] ready]");
}

void Connection ::tick(){
  switch(connection){
      case ConnectionState :: NOT_CONNECTED :
        if(user == User :: DETECTED)
          connection = ConnectionState::CONNECTED;
        break;
        
      case ConnectionState :: CONNECTED :
        if(timeout == 0){
          auc = true; //ask to send a request
          timeout++;
        }else if(!ack){
          timeout ++;
        }else if(ack){
          timeout = 0;
          ack = false;
        }

        if(timeout >=  TIMEOUT){
          switchNotConnected();
        }

        if(user == User::NOT_FOUND){
          switchNotConnected();
        }
        
        break;
  }
  
}

  void Connection :: switchNotConnected(){
    connection = ConnectionState :: NOT_CONNECTED;
          setting = Setting::AUTO;
          timeout = 0;
          setting = Setting::AUTO;
          openPump = false;
          auc = false;
          ack  = false;
  }
