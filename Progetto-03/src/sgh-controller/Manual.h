#ifndef __MANUAL__
#define __MANUAL__

#include "Task.h"
#include "config.h"
#include "Arduino.h"
#include "Enumeration.h"
#include "Led.h"
#include "Pump.h"


extern Setting setting;
extern User user;
extern int currFlowM;
extern bool openPump;
extern float currUmidity;

/**
 * Task for manual sgh-controller behavior.
 */
class Manual : public Task{
  private:
    Led *Lm;
    Pump *pump;
    bool toReset;
    State state;
    void switchToAuto();
    void reset();
  public:
  Manual(int pinLm, Pump *pump);
  void init(int period);
  void tick();

  
};

#endif
