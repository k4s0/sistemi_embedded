#include "DetectMotion.h"
#include "Arduino.h"
#include "config.h"
#include "Enumeration.h"


DetectMotion :: DetectMotion(int pinTrig, int pinEcho) {
  this->pinTrig = pinTrig;
  this->pinEcho = pinEcho;
  count = 0;
}

void DetectMotion :: init(int period) {
  Task::init(period);
  sonar = new Sonar(pinTrig, pinEcho);
  Serial.println("[DetectMotion] ready]");
}

void DetectMotion::tick(){
#ifdef __DEBUG__
  Serial.println("[DetectMotion] distance: "+String(sonar->getDistance()));
#endif
  if(sonar->getDistance() < DIST){
    user = User::DETECTED;
#ifdef __DEBUG__
    Serial.println("[DetectMotion] Detected");
#endif
  } else {
    user = User::NOT_FOUND;
#ifdef __DEBUG__
    Serial.println("[DetectMotion] Not Found");
#endif
  }
  
}
