#include "Task.h"
#include "config.h"
#include "Auto.h"


Auto :: Auto(int pinL1, Pump *pump){
  this->pump = pump;
  L1 = new Led(pinL1);
}

void Auto :: init(int period){
  Serial.println("[Auto] Ready");
  Task::init(period);
  reset();
}

void Auto :: tick(){
  
  if(setting == Setting::AUTO){
    if(toReset) {reset();}
    switch(this->state){
      case State::CLOSE :
        if(openPump == true){
          pump->openPump(currFlow);
          //pump->openPump(i);
          state = State :: OPEN;
        }
        break;
      case State::OPEN :
        cTimeout++;
        if(openPump == true){
          pump->openPump(currFlow);
          if((cTimeout * TICK_AUTO) >= Tmax){
            openPump= false;
            pump->closePump();
            state = State :: CLOSE;
            cTimeout = 0;
            timeout = true;
          }
        } else {
          pump->closePump();
          state = State :: CLOSE;
        }
        break;
    }
  }else if (setting == Setting::MANUAL && !toReset){
    switchToManual();
  }
}

void Auto::reset(){
  state = State :: CLOSE;
  toReset = false;
  cTimeout = 0;
  L1->switchOn();
}

void Auto::switchToManual(){
  toReset = true;
  L1->switchOff();
  pump->closePump();
}
