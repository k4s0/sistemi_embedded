#include "Led.h"
#include "Arduino.h"

Led::Led(int pin) {
  this->pin = pin;
  pinMode(pin, OUTPUT);
}

void Led::switchOn() {
  digitalWrite(pin, HIGH);
}

void Led::switchOff() {
  digitalWrite(pin, LOW);
}

void Led::fade(int value){
  value = value > 255 ? 255 : value;
  value = value < 0 ? 0 : value;
  analogWrite(this->pin, value);
}
