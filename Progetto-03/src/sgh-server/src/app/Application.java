package app;

import logicApplication.LogicApplication;
import serial.SerialComunicator;
import serverHttp.Server;

/**
 * 
 * Launch the whole Application
 *
 */
public class Application {

	private static volatile boolean SERIAL_READY = false;

	public static synchronized void SerialOK() {
		SERIAL_READY = true;
	}

	public static void main(String[] args) {
		try {
			//Create serial comunicator channel
			SerialComunicator c = new SerialComunicator("COM6", 9600);
			c.startCommuncation();
			while (!SERIAL_READY) {
				Thread.sleep(100);
				System.out.print(".");
			}
			System.out.println("Serial Channel is Ready");
			// Create application logic instance.
			LogicApplication la = new LogicApplication(c);
			c.addObserver(la);
			// Create new HTTP server on the provided port.
			Server httpServer = new Server(8080);
			// Start the application logic.
			la.start();
			// Attach LA observer to the server
			httpServer.addObserver(la);
			// Start HTTP server
			httpServer.start();

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
