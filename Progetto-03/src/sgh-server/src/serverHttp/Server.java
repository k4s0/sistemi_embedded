package serverHttp;

import java.io.BufferedReader;

import logicApplication.Humidity;
import utility.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.util.ArrayList;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

public class Server extends Observable {
	private HttpServer server;
	private int port;

	/**
	 * Constructor of the HTTP Server class.
	 * 
	 * @param port specify the server port.
	 * @throws IOException
	 */
	public Server(int port) throws IOException {
		this.port = port;
		// create new HTTP server instance
		this.server = HttpServer.create(new InetSocketAddress(this.port), 0);
		// create new server path resource
		server.createContext("/server", new MyHandler());
		server.setExecutor(null); // creates a default executor

	}

	/**
	 * Start the HTTP server.
	 */
	public void start() {
		server.start();
		System.out.println("[Server] Server On");
	}

	/**
	 * 
	 * This class handle the messages provided by the EDGE.
	 *
	 */
	class MyHandler implements HttpHandler {

		private ArrayList<Double> lstValue = new ArrayList<>();
		public void handle(HttpExchange t) throws IOException {
			String response = "Server Ok";
			t.sendResponseHeaders(200, response.length());
			InputStream input = t.getRequestBody();
			StringBuilder stringBuilder = new StringBuilder();

			new BufferedReader(new InputStreamReader(input)).lines()
					.forEach((String s) -> stringBuilder.append(s + "\n"));
			System.out.println("[Server] Message from EDGE: " + Double.valueOf(stringBuilder.toString()));
			// Notify the observer about the incoming value
			notifyEvent(new Humidity(Double.valueOf(stringBuilder.toString()) / 100));
			// Every minute memorize the average value of humidity into DB
			if (lstValue.size() <= 120) {
				lstValue.add(Double.valueOf(stringBuilder.toString()) / 100);
			} else {
				Double avg = lstValue.stream().mapToDouble(val -> val).average().orElse(0.0);
				System.out.println("[Server] Avg humidity value: " + avg);
				DBInstance.getDB().saveHumidity(avg);
				lstValue.clear();
			}
			OutputStream os = t.getResponseBody();
			os.write(response.getBytes());
			os.close();
		}
	}
}
