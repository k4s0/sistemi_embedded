package utility;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

import logicApplication.Supply;

public class DBInstance {
	private static DBInstance SINGLETON = null;
	private static String url = "jdbc:mysql://localhost:3306/sgh?useSSL=false&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
	private static String user = "root";
	private static String psw = "";

	private DBInstance() {
	}

	/**
	 * @return DB instance
	 */
	public static DBInstance getDB() {
		if (SINGLETON == null) {
			SINGLETON = new DBInstance();
		}
		return SINGLETON;
	}

	/**
	 * @param value humidity value to save
	 */
	public void saveHumidity(double value) {
		// Query string
		String sql = "INSERT INTO humidity(value) VALUES(?)";

		try (Connection con = DriverManager.getConnection(url, user, psw);
				PreparedStatement pst = con.prepareStatement(sql)) {

			// pst.setFloat(1, value);
			pst.setDouble(1, value);
			pst.executeUpdate();

			System.out.println("[Server][JDBC] A new humidity value has been insert !");

		} catch (SQLException ex) {

			Logger lgr = Logger.getLogger(DBInstance.class.getName());
			lgr.log(Level.SEVERE, ex.getMessage(), ex);

		}
	}

	/**
	 * notify to DB that a warning occurred
	 */
	public void saveWarning() {
		// Query string
		String sql = "INSERT INTO warning(status) VALUES(?)";

		try (Connection con = DriverManager.getConnection(url, user, psw);
				PreparedStatement pst = con.prepareStatement(sql)) {
			pst.setString(1, "Warning Timeout !");
			pst.executeUpdate();

			System.out.println("[Server][JDBC] A new warning log has been insert !");

		} catch (SQLException ex) {

			Logger lgr = Logger.getLogger(DBInstance.class.getName());
			lgr.log(Level.SEVERE, ex.getMessage(), ex);

		}
	}

	/**
	 * @param s supply instance to insert
	 */
	public void insertSupply(Supply s) {
		String sql = "INSERT INTO supply(ero_start, ero_end, ero_time) VALUES(?, ?, ?)";

		try (Connection con = DriverManager.getConnection(url, user, psw);
				PreparedStatement pst = con.prepareStatement(sql)) {

			pst.setTimestamp(1, s.getStart());
			pst.setTimestamp(2, s.getEnd());
			pst.setInt(3, s.duration());
			pst.executeUpdate();

			System.out.println("[Server][JDBC] A new supply log has been insert !");

		} catch (SQLException ex) {

			Logger lgr = Logger.getLogger(DBInstance.class.getName());
			lgr.log(Level.SEVERE, ex.getMessage(), ex);

		}
	}

}
