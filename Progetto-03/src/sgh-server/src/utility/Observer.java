package utility;

public interface Observer {

	boolean notifyEvent(Event ev);
}
