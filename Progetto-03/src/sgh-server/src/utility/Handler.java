package utility;

public interface Handler {
	void handle(Event ev);
}
