package logicApplication;

import utility.Event;

/**
 * The Event that show the humidity level.
 *
 */
public class Humidity extends Event {
	private double u;

	public Humidity(double u) {
		this.u = u;
	}

	public double getHumidity() {
		return this.u;
	}
}
