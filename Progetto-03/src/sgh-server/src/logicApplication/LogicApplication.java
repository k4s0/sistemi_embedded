package logicApplication;

import java.util.Optional;
import serial.SerialComunicator;
import serial.TypeMsg;
import utility.*;

/**
 * Logic application.
 *
 */
public class LogicApplication extends EventLoopControllerWithHandlers {

	private static final double Umin = 0.30;
	private static final double DeltaU = 0.05;
	private final SerialComunicator channel;
	private Optional<Supply> supply = Optional.empty();

	private enum State {
		MANUAL, WAIT, EROG
	}

	private State currState;

	public LogicApplication(final SerialComunicator channel) throws Exception {
		currState = State.WAIT;
		this.channel = channel;
	}

	@Override
	public synchronized void start() {
		super.start();
		System.out.println("[Logic Application] LA start");
	}

	@SuppressWarnings("incomplete-switch")
	@Override
	protected void setupHandlers() {
		addHandler(Humidity.class, (Event ev) -> {
			System.out.println("Status: " + currState);
			Double u = ((Humidity) ev).getHumidity();
			switch (currState) {
			case MANUAL: {
				System.out.println("[Logic Application] I'm in Manual");
				System.out.println("[Logic Application] Sent Umidity value " + u);
				break;
			}
			case WAIT: {
				System.out.println("[Logic Application]" + u);
				if (u < Umin) {
					currState = State.EROG;
					channel.sendMsg(getFlow(u).getId());
					supply = Optional.of(new Supply());
				}
				break;
			}

			case EROG: {
				if (u < Umin) {
					channel.sendMsg(getFlow(u).getId());
				}
				if (isUmidityCorrect(u)) {
					channel.sendMsg(TypeMsg.CLOSE.getId());
					currState = State.WAIT;
					if(supply.isPresent()) {
						supply.get().end();
						DBInstance.getDB().insertSupply(supply.get());
					}
				}
				break;
			}
			}
			channel.sendMsg(TypeMsg.UMIDITY.getId() + u);
		}).addHandler(ArduinoEvent.class, (Event ev) -> {
			TypeMsg type = ((ArduinoEvent) ev).getType();
			switch (type) {
			case MANUAL:
				currState = State.MANUAL;
				if(supply.isPresent()) {
					supply.get().end();
					DBInstance.getDB().insertSupply(supply.get());
				}
				break;
			case AUTO:
				currState = State.WAIT;
				break;
			case TIMEOUT:
				currState = State.WAIT;
				DBInstance.getDB().saveWarning();
				if(supply.isPresent()) {
					supply.get().end();
					DBInstance.getDB().insertSupply(supply.get());
				}
			}
		});
	}

	private TypeMsg getFlow(final double u) {
		TypeMsg msg = TypeMsg.Pmin;
		msg = u < 0.20 ? TypeMsg.Pmed : msg;
		return u < 0.10 ? TypeMsg.Pmax : msg;

	}

	private boolean isUmidityCorrect(final double u) {
		return u >= (Umin + DeltaU);
	}

}
