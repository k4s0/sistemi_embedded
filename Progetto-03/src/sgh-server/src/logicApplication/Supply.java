package logicApplication;

import java.sql.Timestamp;


/**
 * Represent water supply information.
 *
 */
public class Supply {
	private Timestamp start;
	private long tS;
	private Timestamp end;
	private long tE;
	
	public Supply() {
		this.tS = System.currentTimeMillis();
		this.start = new Timestamp(this.tS);
	}
	
	/**
	 * The supply end and the object will be completed.
	 */
	public void end() {
		this.tE = System.currentTimeMillis();
		this.end = new Timestamp(this.tE);
	}

	public Timestamp getStart() {
		return start;
	}

	public Timestamp getEnd() {
		return end;
	}
	
	public int duration() {
		return (int)(this.tE / 1000 - this.tS/1000);
	}

	
	
	
	

}
