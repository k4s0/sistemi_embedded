package logicApplication;

import serial.TypeMsg;
import utility.Event;


/**
 * The event that could come from Arduino platform
 *
 */
public class ArduinoEvent extends Event{
	private TypeMsg type;
	
	public ArduinoEvent(final TypeMsg type) {
		this.type = type;
	}
	
	public TypeMsg getType() {
		return this.type;
	}
	

}
