package serial;

import java.util.Arrays;
import java.util.Optional;

import logicApplication.ArduinoEvent;

/**
 * All possible massages between Arduino and PC except number.
 *
 */
public enum TypeMsg {
	Pmin {

		@Override
		public String getId() {
			return "1";
		}
	},
	Pmed {

		@Override
		public String getId() {
			return "2";
		}
	},
	Pmax {

		@Override
		public String getId() {
			return "3";
		}

	},
	CLOSE {

		@Override
		public String getId() {
			return "c";
		}
	},

	TIMEOUT {

		@Override
		public String getId() {
			return "t";
		}

	},

	AUTO {

		@Override
		public String getId() {
			return "a";
		}
	},

	MANUAL {

		@Override
		public String getId() {
			return "m";
		}

	},

	UMIDITY {

		@Override
		public String getId() {
			return "u";
		}
	};

	/**
	 * @return message identifier
	 */
	abstract public String getId();

	/**
	 * Check if c is a message identifier.
	 * 
	 * @param c message received
	 * @return true if is an identifier
	 */
	public static boolean isACorrectMessage(String c) {
		return Arrays.asList(TypeMsg.values()).stream().anyMatch(i -> i.getId().equals(c));
	}

	/**
	 * When a message come from arduino this method parse it in a message that logic application can understand
	 * @param s the string that come from arduino
	 * @return the new message
	 */
	public static ArduinoEvent translateMassage(final String s) {
		Optional<TypeMsg> msg = Arrays.asList(TypeMsg.values()).stream().filter(i -> i.getId().equals(s)).findFirst();
		return msg.isPresent() ? new ArduinoEvent(msg.get()) : null;

	}

}
