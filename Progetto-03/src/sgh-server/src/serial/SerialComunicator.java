package serial;

import app.Application;
import utility.Observable;

/**
 * Class handle the comunications with sgh-controller.
 */
public class SerialComunicator extends Observable{

	private SerialCommChannel serialChannel;
	
	public SerialComunicator(String port, int rate) throws Exception {
		this.serialChannel = new SerialCommChannel(port, rate);
	}
	
	/**
	 * Send Message to Arduino.
	 * 
	 * @param msg message to send
	 */
	public void sendMsg(final String msg) {
		serialChannel.sendMsg(msg);
		System.out.println("send " + msg);
	}

	/**
	 * Start arduino communication thread
	 */
	public boolean startCommuncation() {
		Thread t = new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					System.out.println("Waiting Arduino for rebooting...");
					Thread.sleep(4000);
					Application.SerialOK();
					while (true) {
						if (serialChannel.isMsgAvailable()) {
							String msg = serialChannel.receiveMsg().trim();
							System.out.println(msg);
							if(TypeMsg.isACorrectMessage(msg)) {
								SerialComunicator.this.notifyEvent(TypeMsg.translateMassage(msg));
							}
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		t.start();
		return true;
	}
}
