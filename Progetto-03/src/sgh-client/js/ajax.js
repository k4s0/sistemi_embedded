$(document).ready(function(){
        $.ajax({
            type:'POST',
            url:'php/fetchHumidityData.php',
            data:{fetchHumidityData:'1'},
            success:function(data){
                var json = $.parseJSON(data);
                var table_h = '<h1>SGH Humidity History</h1><table class="table">'+
                            '<thead><tr><th>ID</th><th>Humidity Value</th><th>Date</th></tr></thead><tbody>';
                            for(i=0;i<json.length;++i){
                                table_h += '<tr><td>' + json[i].id + '</td><td>' + json[i].value*100+'%' + '</td><td>' + json[i].hours +'</td></tr>';
                            }
                            table_h += '</tbody></table>';
                $('#table_h').html(table_h);
            }
        });
        $.ajax({
            type:'POST',
            url:'php/fetchEroData.php',
            data:{fetchEroData:'1'},
            success:function(data){
                var json = $.parseJSON(data);
                var table_s = '<h1>SGH Water Erogation</h1><table class="table">'+
                            '<thead><tr><th>Start</th><th>Stop</th><th>Total Erogation Time</th></tr></thead><tbody>';
                            for(i=0;i<json.length;++i){
                                table_s += '<tr><td>' + json[i].ero_start + '</td><td>' + json[i].ero_end + '</td><td>' + json[i].ero_time +'</td></tr>';
                            }
                            table_s += '</tbody></table>';
                $('#table_s').html(table_s);
            }
        });

    $.ajax({
        type:'POST',
        url:'php/fetchWarningsData.php',
        data:{fetchWarningsData:'1'},
        success:function(data){
            console.log(data)
            var json = $.parseJSON(data);
            var table_h = '<h1>SGH Warning History</h1><table class="table">'+
                '<thead><tr><th>ID</th><th>Warnings</th><th>Date</th></tr></thead><tbody>';
            for(i=0;i<json.length;++i){
                table_h += '<tr><td>' + json[i].id + '</td><td>' + json[i].time + '</td><td>' + json[i].status +'</td></tr>';
            }
            table_h += '</tbody></table>';
            $('#table_w').html(table_h);
        }
    });
        setInterval(function () {
           $.ajax({
            type:'POST',
            url:'php/fetchHumidityData.php',
            data:{fetchHumidityData:'1'},
            success:function(data){
                var json = $.parseJSON(data);
                var table_h = '<h1>SGH Humidity History</h1><table class="table">'+
                            '<thead><tr><th>ID</th><th>Humidity Value</th><th>Date</th></tr></thead><tbody>';
                            for(i=0;i<json.length;++i){
                                table_h += '<tr><td>' + json[i].id + '</td><td>' + json[i].value*100+'%' + '</td><td>' + json[i].hours +'</td></tr>';
                            }
                            table_h += '</tbody></table>';
                $('#table_h').html(table_h);
            }

        });
           $.ajax({
            type:'POST',
            url:'php/fetchEroData.php',
            data:{fetchEroData:'1'},
            success:function(data){
                var json = $.parseJSON(data);
                var table_s = '<h1>SGH Water Erogation</h1><table class="table">'+
                            '<thead><tr><th>Start</th><th>Stop</th><th>Total Erogation Time</th></tr></thead><tbody>';
                            for(i=0;i<json.length;++i){
                                table_s += '<tr><td>' + json[i].ero_start + '</td><td>' + json[i].ero_end + '</td><td>' + json[i].ero_time +'</td></tr>';
                            }
                            table_s += '</tbody></table>';
                $('#table_s').html(table_s);
            }
        });
            $.ajax({
                type:'POST',
                url:'php/fetchWarningsData.php',
                data:{fetchWarningsData:'1'},
                success:function(data){
                    console.log(data)
                    var json = $.parseJSON(data);
                    var table_h = '<h1>SGH Warning History</h1><table class="table">'+
                        '<thead><tr><th>ID</th><th>Warnings</th><th>Date</th></tr></thead><tbody>';
                    for(i=0;i<json.length;++i){
                        table_h += '<tr><td>' + json[i].id + '</td><td>' + json[i].time + '</td><td>' + json[i].status +'</td></tr>';
                    }
                    table_h += '</tbody></table>';
                    $('#table_w').html(table_h);
                }
            });
        }, 10000);
});