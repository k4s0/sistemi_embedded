-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Feb 20, 2019 at 02:08 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sgh`
--

-- --------------------------------------------------------

--
-- Table structure for table `humidity`
--

CREATE TABLE `humidity` (
  `id` int(3) NOT NULL,
  `value` float NOT NULL,
  `hours` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `humidity`
--

INSERT INTO `humidity` (`id`, `value`, `hours`) VALUES
(1, 0.8, '2019-01-28 23:35:29'),
(2, 0.6, '2019-01-28 23:36:34'),
(3, 0.4, '2019-01-28 23:50:35'),
(4, 0.89, '2019-02-06 01:10:28'),
(5, 0.45, '2019-02-06 01:26:05'),
(17, 1, '2019-02-06 18:10:39'),
(18, 1, '2019-02-06 18:14:49'),
(19, 1, '2019-02-06 18:20:39'),
(20, 1, '2019-02-06 18:24:50'),
(21, 1, '2019-02-06 18:27:47'),
(22, 1, '2019-02-06 18:35:56');

-- --------------------------------------------------------

--
-- Table structure for table `supply`
--

CREATE TABLE `supply` (
  `ero_start` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ero_end` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ero_time` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `supply`
--

INSERT INTO `supply` (`ero_start`, `ero_end`, `ero_time`) VALUES
('2019-02-09 10:26:16', '2019-02-09 10:26:17', 1),
('2019-02-09 10:27:23', '2019-02-09 10:27:24', 1),
('2019-02-09 10:27:26', '2019-02-09 10:27:27', 1),
('2019-02-09 10:27:28', '2019-02-09 10:27:29', 1);

-- --------------------------------------------------------

--
-- Table structure for table `warning`
--

CREATE TABLE `warning` (
  `id` int(4) NOT NULL,
  `time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` varchar(30) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `warning`
--

INSERT INTO `warning` (`id`, `time`, `status`) VALUES
(1, '2019-02-05 23:00:01', 'Warning Timeout !'),
(2, '2019-02-06 01:07:19', 'Warning Timeout !'),
(3, '2019-02-06 01:32:08', 'Warning Timeout !'),
(4, '2019-02-06 14:11:54', 'Warning Timeout !'),
(5, '2019-02-06 14:11:59', 'Warning Timeout !'),
(6, '2019-02-06 14:12:04', 'Warning Timeout !'),
(7, '2019-02-06 14:12:10', 'Warning Timeout !'),
(8, '2019-02-06 14:12:25', 'Warning Timeout !'),
(9, '2019-02-06 14:12:31', 'Warning Timeout !'),
(10, '2019-02-06 14:12:36', 'Warning Timeout !'),
(11, '2019-02-06 15:49:25', 'Warning Timeout !'),
(12, '2019-02-06 15:49:30', 'Warning Timeout !'),
(13, '2019-02-06 15:49:35', 'Warning Timeout !'),
(14, '2019-02-06 15:49:41', 'Warning Timeout !'),
(15, '2019-02-06 15:50:56', 'Warning Timeout !'),
(16, '2019-02-06 15:51:01', 'Warning Timeout !'),
(17, '2019-02-06 15:51:07', 'Warning Timeout !'),
(18, '2019-02-06 15:54:07', 'Warning Timeout !'),
(19, '2019-02-06 15:54:12', 'Warning Timeout !'),
(20, '2019-02-06 15:55:32', 'Warning Timeout !'),
(21, '2019-02-06 15:55:38', 'Warning Timeout !'),
(22, '2019-02-06 15:55:53', 'Warning Timeout !'),
(23, '2019-02-06 15:55:59', 'Warning Timeout !'),
(24, '2019-02-06 15:59:15', 'Warning Timeout !'),
(25, '2019-02-06 15:59:21', 'Warning Timeout !'),
(26, '2019-02-06 16:02:39', 'Warning Timeout !'),
(27, '2019-02-06 16:02:45', 'Warning Timeout !'),
(28, '2019-02-06 16:02:51', 'Warning Timeout !'),
(29, '2019-02-06 16:02:56', 'Warning Timeout !'),
(30, '2019-02-06 16:03:02', 'Warning Timeout !'),
(31, '2019-02-06 16:03:07', 'Warning Timeout !'),
(32, '2019-02-06 16:03:12', 'Warning Timeout !'),
(33, '2019-02-06 16:03:18', 'Warning Timeout !'),
(34, '2019-02-06 16:05:22', 'Warning Timeout !'),
(35, '2019-02-06 16:05:27', 'Warning Timeout !'),
(36, '2019-02-06 16:05:44', 'Warning Timeout !'),
(37, '2019-02-06 16:05:50', 'Warning Timeout !'),
(38, '2019-02-06 16:05:55', 'Warning Timeout !'),
(39, '2019-02-06 16:06:00', 'Warning Timeout !'),
(40, '2019-02-06 16:09:02', 'Warning Timeout !'),
(41, '2019-02-06 16:09:15', 'Warning Timeout !'),
(42, '2019-02-06 16:09:20', 'Warning Timeout !'),
(43, '2019-02-06 16:09:26', 'Warning Timeout !'),
(44, '2019-02-06 16:09:31', 'Warning Timeout !'),
(45, '2019-02-06 16:09:36', 'Warning Timeout !'),
(46, '2019-02-06 16:09:42', 'Warning Timeout !'),
(47, '2019-02-06 16:09:47', 'Warning Timeout !'),
(48, '2019-02-06 16:09:52', 'Warning Timeout !'),
(49, '2019-02-06 16:13:16', 'Warning Timeout !'),
(50, '2019-02-06 16:13:21', 'Warning Timeout !'),
(51, '2019-02-06 16:13:26', 'Warning Timeout !'),
(52, '2019-02-06 16:13:32', 'Warning Timeout !'),
(53, '2019-02-06 16:17:41', 'Warning Timeout !'),
(54, '2019-02-06 16:17:46', 'Warning Timeout !'),
(55, '2019-02-06 16:17:52', 'Warning Timeout !'),
(56, '2019-02-06 16:20:29', 'Warning Timeout !'),
(57, '2019-02-06 16:20:34', 'Warning Timeout !'),
(58, '2019-02-06 16:20:40', 'Warning Timeout !'),
(59, '2019-02-06 16:20:45', 'Warning Timeout !'),
(60, '2019-02-06 16:20:51', 'Warning Timeout !'),
(61, '2019-02-06 16:20:57', 'Warning Timeout !'),
(62, '2019-02-06 16:21:07', 'Warning Timeout !'),
(63, '2019-02-06 16:21:21', 'Warning Timeout !'),
(64, '2019-02-06 16:21:27', 'Warning Timeout !'),
(65, '2019-02-06 16:21:32', 'Warning Timeout !'),
(66, '2019-02-06 16:23:31', 'Warning Timeout !'),
(67, '2019-02-06 16:23:36', 'Warning Timeout !'),
(68, '2019-02-06 16:23:41', 'Warning Timeout !'),
(69, '2019-02-06 16:23:47', 'Warning Timeout !'),
(70, '2019-02-06 16:23:52', 'Warning Timeout !'),
(71, '2019-02-06 16:23:57', 'Warning Timeout !'),
(72, '2019-02-06 16:24:03', 'Warning Timeout !'),
(73, '2019-02-06 16:24:08', 'Warning Timeout !'),
(74, '2019-02-06 16:29:21', 'Warning Timeout !'),
(75, '2019-02-06 16:29:33', 'Warning Timeout !'),
(76, '2019-02-06 16:29:43', 'Warning Timeout !'),
(77, '2019-02-06 16:29:49', 'Warning Timeout !'),
(78, '2019-02-06 16:29:55', 'Warning Timeout !'),
(79, '2019-02-06 16:30:01', 'Warning Timeout !'),
(80, '2019-02-06 16:33:18', 'Warning Timeout !'),
(81, '2019-02-06 16:33:23', 'Warning Timeout !'),
(82, '2019-02-06 16:33:28', 'Warning Timeout !'),
(83, '2019-02-06 16:33:34', 'Warning Timeout !'),
(84, '2019-02-06 16:33:39', 'Warning Timeout !'),
(85, '2019-02-06 16:37:15', 'Warning Timeout !'),
(86, '2019-02-06 16:37:20', 'Warning Timeout !'),
(87, '2019-02-06 16:37:26', 'Warning Timeout !'),
(88, '2019-02-06 16:37:31', 'Warning Timeout !'),
(89, '2019-02-06 16:37:37', 'Warning Timeout !'),
(90, '2019-02-06 16:37:42', 'Warning Timeout !'),
(91, '2019-02-06 16:37:47', 'Warning Timeout !'),
(92, '2019-02-06 16:37:53', 'Warning Timeout !'),
(93, '2019-02-06 16:37:58', 'Warning Timeout !'),
(94, '2019-02-06 16:45:56', 'Warning Timeout !'),
(95, '2019-02-06 16:46:01', 'Warning Timeout !'),
(96, '2019-02-06 16:46:06', 'Warning Timeout !'),
(97, '2019-02-06 16:48:31', 'Warning Timeout !'),
(98, '2019-02-06 16:48:36', 'Warning Timeout !'),
(99, '2019-02-06 16:49:53', 'Warning Timeout !'),
(100, '2019-02-06 16:52:21', 'Warning Timeout !'),
(101, '2019-02-06 16:53:39', 'Warning Timeout !'),
(102, '2019-02-06 16:55:21', 'Warning Timeout !'),
(103, '2019-02-06 16:55:26', 'Warning Timeout !'),
(104, '2019-02-06 16:55:32', 'Warning Timeout !'),
(105, '2019-02-06 16:55:37', 'Warning Timeout !'),
(106, '2019-02-06 16:55:42', 'Warning Timeout !'),
(107, '2019-02-06 16:56:44', 'Warning Timeout !'),
(108, '2019-02-06 16:56:49', 'Warning Timeout !'),
(109, '2019-02-06 16:56:54', 'Warning Timeout !'),
(110, '2019-02-06 16:57:00', 'Warning Timeout !'),
(111, '2019-02-06 16:58:53', 'Warning Timeout !'),
(112, '2019-02-06 16:59:05', 'Warning Timeout !'),
(113, '2019-02-06 17:01:04', 'Warning Timeout !'),
(114, '2019-02-06 17:01:09', 'Warning Timeout !'),
(115, '2019-02-06 17:01:14', 'Warning Timeout !'),
(116, '2019-02-06 17:01:24', 'Warning Timeout !'),
(117, '2019-02-06 17:01:29', 'Warning Timeout !'),
(118, '2019-02-06 17:01:37', 'Warning Timeout !'),
(119, '2019-02-06 17:02:09', 'Warning Timeout !'),
(120, '2019-02-06 17:02:14', 'Warning Timeout !'),
(121, '2019-02-06 17:02:19', 'Warning Timeout !'),
(122, '2019-02-06 17:02:25', 'Warning Timeout !'),
(123, '2019-02-06 17:02:44', 'Warning Timeout !'),
(124, '2019-02-06 17:02:50', 'Warning Timeout !'),
(125, '2019-02-06 17:02:55', 'Warning Timeout !'),
(126, '2019-02-06 17:03:00', 'Warning Timeout !'),
(127, '2019-02-06 17:03:06', 'Warning Timeout !'),
(128, '2019-02-06 17:03:11', 'Warning Timeout !'),
(129, '2019-02-06 17:03:17', 'Warning Timeout !'),
(130, '2019-02-06 17:03:22', 'Warning Timeout !'),
(131, '2019-02-06 17:03:27', 'Warning Timeout !'),
(132, '2019-02-06 17:05:11', 'Warning Timeout !'),
(133, '2019-02-06 17:05:16', 'Warning Timeout !'),
(134, '2019-02-06 17:05:21', 'Warning Timeout !'),
(135, '2019-02-06 17:05:27', 'Warning Timeout !'),
(136, '2019-02-06 17:09:10', 'Warning Timeout !'),
(137, '2019-02-06 17:09:15', 'Warning Timeout !'),
(138, '2019-02-06 17:09:21', 'Warning Timeout !'),
(139, '2019-02-06 17:10:07', 'Warning Timeout !'),
(140, '2019-02-06 17:10:33', 'Warning Timeout !'),
(141, '2019-02-06 17:10:39', 'Warning Timeout !'),
(142, '2019-02-06 17:10:44', 'Warning Timeout !'),
(143, '2019-02-06 17:10:49', 'Warning Timeout !'),
(144, '2019-02-06 17:10:55', 'Warning Timeout !'),
(145, '2019-02-06 17:15:54', 'Warning Timeout !'),
(146, '2019-02-06 17:15:59', 'Warning Timeout !'),
(147, '2019-02-06 17:16:05', 'Warning Timeout !'),
(148, '2019-02-06 17:16:11', 'Warning Timeout !'),
(149, '2019-02-06 17:16:16', 'Warning Timeout !'),
(150, '2019-02-06 17:28:06', 'Warning Timeout !'),
(151, '2019-02-06 17:52:24', 'Warning Timeout !'),
(152, '2019-02-06 17:52:29', 'Warning Timeout !'),
(153, '2019-02-06 17:52:35', 'Warning Timeout !'),
(154, '2019-02-06 17:52:41', 'Warning Timeout !'),
(155, '2019-02-06 17:52:47', 'Warning Timeout !'),
(156, '2019-02-06 17:52:52', 'Warning Timeout !'),
(157, '2019-02-06 17:52:58', 'Warning Timeout !'),
(158, '2019-02-06 17:54:12', 'Warning Timeout !'),
(159, '2019-02-06 17:55:01', 'Warning Timeout !'),
(160, '2019-02-06 17:55:14', 'Warning Timeout !'),
(161, '2019-02-06 17:55:19', 'Warning Timeout !'),
(162, '2019-02-06 17:55:25', 'Warning Timeout !'),
(163, '2019-02-06 17:55:31', 'Warning Timeout !'),
(164, '2019-02-06 17:55:36', 'Warning Timeout !'),
(165, '2019-02-06 17:55:42', 'Warning Timeout !'),
(166, '2019-02-06 17:55:47', 'Warning Timeout !'),
(167, '2019-02-06 17:55:53', 'Warning Timeout !'),
(168, '2019-02-06 17:55:59', 'Warning Timeout !'),
(169, '2019-02-06 17:56:04', 'Warning Timeout !'),
(170, '2019-02-06 17:56:10', 'Warning Timeout !'),
(171, '2019-02-06 17:56:15', 'Warning Timeout !'),
(172, '2019-02-06 17:56:21', 'Warning Timeout !'),
(173, '2019-02-06 17:56:26', 'Warning Timeout !'),
(174, '2019-02-06 17:56:32', 'Warning Timeout !'),
(175, '2019-02-06 17:57:16', 'Warning Timeout !'),
(176, '2019-02-06 17:57:23', 'Warning Timeout !'),
(177, '2019-02-06 17:57:49', 'Warning Timeout !'),
(178, '2019-02-06 17:58:59', 'Warning Timeout !');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `humidity`
--
ALTER TABLE `humidity`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `supply`
--
ALTER TABLE `supply`
  ADD PRIMARY KEY (`ero_start`);

--
-- Indexes for table `warning`
--
ALTER TABLE `warning`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `humidity`
--
ALTER TABLE `humidity`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `warning`
--
ALTER TABLE `warning`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=179;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
