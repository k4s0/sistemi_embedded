package unibo.btclient;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;

import java.util.UUID;

import unibo.btclient.utils.C;
import unibo.btlib.BluetoothChannel;
import unibo.btlib.ConnectionTask;
import unibo.btlib.RealBluetoothChannel;
import unibo.btlib.ConnectToBluetoothServerTask;
import unibo.btlib.BluetoothUtils;
import unibo.btlib.exceptions.BluetoothDeviceNotFound;

public class MainActivity extends AppCompatActivity {

    private BluetoothChannel btChannel;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final BluetoothAdapter btAdapter = BluetoothAdapter.getDefaultAdapter();

        if(btAdapter != null && !btAdapter.isEnabled()){
            startActivityForResult(new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE), C.bluetooth.ENABLE_BT_REQUEST);
        }
        initUI();
    }

    private void initUI() {
        findViewById(R.id.openclose).setEnabled(false);
        findViewById(R.id.editText).setEnabled(false);
        findViewById(R.id.sendBtn).setEnabled(false);
        findViewById(R.id.manual).setEnabled(false);

        findViewById(R.id.connectBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    connectToBTServer();
                    ((TextView) findViewById(R.id.statusLabel)).setText(String.format("Connection in progress..."));
                } catch (BluetoothDeviceNotFound bluetoothDeviceNotFound) {
                    bluetoothDeviceNotFound.printStackTrace();
                }
            }
        });

        findViewById(R.id.sendBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String message = ((EditText)findViewById(R.id.editText)).getText().toString();
                btChannel.sendMessage(message);
                ((EditText)findViewById(R.id.editText)).setText("");
            }
        });

        findViewById(R.id.manual).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String message;
                if(((Switch)findViewById(R.id.manual)).isChecked()){
                    message = "setManual";
                    findViewById(R.id.openclose).setEnabled(true);
                    if(((Switch)findViewById(R.id.openclose)).isChecked()){
                        findViewById(R.id.editText).setEnabled(true);
                        findViewById(R.id.sendBtn).setEnabled(true);
                    }
                } else {
                    message = "setAuto";
                    findViewById(R.id.openclose).setEnabled(false);
                    findViewById(R.id.editText).setEnabled(false);
                    findViewById(R.id.sendBtn).setEnabled(false);
                }
                btChannel.sendMessage(message);
            }
        });

        findViewById(R.id.openclose).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String message;
                if(((Switch)findViewById(R.id.openclose)).isChecked()){
                    message = "open";
                    findViewById(R.id.editText).setEnabled(true);
                    findViewById(R.id.sendBtn).setEnabled(true);
                } else {
                    message = "close";
                    findViewById(R.id.editText).setEnabled(false);
                    findViewById(R.id.sendBtn).setEnabled(false);
                }
                btChannel.sendMessage(message);
            }
        });
    }

    @Override
    protected void onStop() {
        super.onStop();
        btChannel.close();
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, @Nullable final Intent data) {
        if(requestCode == C.bluetooth.ENABLE_BT_REQUEST && resultCode == RESULT_OK){
            Log.d(C.APP_LOG_TAG, "Bluetooth enabled!");
        }

        if(requestCode == C.bluetooth.ENABLE_BT_REQUEST && resultCode == RESULT_CANCELED){
            Log.d(C.APP_LOG_TAG, "Bluetooth not enabled!");
        }
    }

    private void connectToBTServer() throws BluetoothDeviceNotFound {
        final BluetoothDevice serverDevice = BluetoothUtils.getPairedDeviceByName(C.bluetooth.BT_DEVICE_ACTING_AS_SERVER_NAME);
        final UUID uuid = BluetoothUtils.getEmbeddedDeviceDefaultUuid();

        AsyncTask<Void, Void, Integer> execute = new ConnectToBluetoothServerTask(serverDevice, uuid, new ConnectionTask.EventListener() {
            @Override
            public void onConnectionActive(final BluetoothChannel channel) {

                ((TextView) findViewById(R.id.statusLabel)).setText(String.format("Status : connected to server on device %s",
                        serverDevice.getName()));

                findViewById(R.id.connectBtn).setEnabled(false);
                findViewById(R.id.manual).setEnabled(true);

                btChannel = channel;
                btChannel.registerListener(new RealBluetoothChannel.Listener() {
                    @Override
                    public void onMessageReceived(String receivedMessage) {
                        if(receivedMessage.indexOf('u')==0){
                            ((TextView) findViewById(R.id.humidity)).setText(receivedMessage.substring(1, receivedMessage.length()));
                        } else if(receivedMessage.indexOf('a')==0 && receivedMessage.indexOf('u')==1 && receivedMessage.indexOf('c')==2) {
                            String message = "imc";
                            btChannel.sendMessage(message);
                            ((TextView) findViewById(R.id.chatLabel)).setText("Ho ricevuto auc e rispondo imc");
                        } else {
                            ((TextView) findViewById(R.id.chatLabel)).append(String.format("> [RECEIVED from %s] %s\n",
                                    btChannel.getRemoteDeviceName(),
                                    receivedMessage));
                        }
                    }

                    @Override
                    public void onMessageSent(String sentMessage) {
                        ((TextView) findViewById(R.id.chatLabel)).append(String.format("> [SENT to %s] %s\n",
                                btChannel.getRemoteDeviceName(),
                                sentMessage));
                    }
                });
            }

            @Override
            public void onConnectionCanceled() {
                ((TextView) findViewById(R.id.statusLabel)).setText(String.format("Status : unable to connect, device %s not found!",
                        C.bluetooth.BT_DEVICE_ACTING_AS_SERVER_NAME));
            }
        }).execute();
    }
}
